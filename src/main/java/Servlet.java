import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("first name");
        String lastName = request.getParameter("last name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        PrintWriter out = response.getWriter();
        out.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
        out.println("<h1>Database Contents</h1>");
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h2>Welcome " + firstName + " " + lastName + "</h2>");

        finalDAO f = finalDAO.getInstance();
        List<User> c = f.getUsers();
        for (User i : c) {
            out.println("<p>User: " + i + "</p>");
        }

        Session session = HibernateUtilities.getSessionFactory().openSession();
        session.beginTransaction();

        //enters the user input into the database by calling the set methods in the User class
        User newUser = new User();
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setPassword(password);

        //Saves and commits the user input to the database
        session.save(newUser);
        session.getTransaction().commit();

        //Displays the last result in the database to inform the user that their entry has been added
        Object result = session.createSQLQuery("SELECT user_id FROM user_table ORDER BY user_id DESC LIMIT 1;")
                .uniqueResult();
        int lastID = (Integer) result;

        String sql = "FROM User WHERE id = " + (lastID);
        User lastUser = (User) session.createQuery(sql).getSingleResult();

        //Closes out the hibernate utility
        HibernateUtilities.shutdown();

        out.println("<h2>User " + lastUser + " has been added</h2>");
        out.println("<p>Json Output: " + userToJSON(newUser) + "</p>");
        out.println("</body></html>");
    }

    //If the webpage encounters an error
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<h1>Error</h1>");
        out.println("<h1>Webpage failure to load</h1>");
    }

    //Code to convert SQL return to JSON output
    public static String userToJSON(User user) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }

}

