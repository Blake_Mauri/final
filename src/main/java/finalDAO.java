import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class finalDAO {
    SessionFactory factory = null;
    Session session = null;

    private static finalDAO single_instance = null;
    private finalDAO()
    {
        factory = HibernateUtilities.getSessionFactory();
    }
    public static finalDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new finalDAO();
        }
        return single_instance;
    }

    public List<User> getUsers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from User";
            List<User> cs = (List<User>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    //Used to get a single user from database
    public User getUser(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from User where id=" + Integer.toString(id);
            User c = (User)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
